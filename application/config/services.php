<?php

declare(strict_types=1);

use App\Domains\Interfaces\OrderRepoInterface;
use App\Domains\Interfaces\SellerRepoInterface;
use App\Domains\Repositories\OrderRepository;
use App\Domains\Repositories\SellerRepository;
use Laminas\Diactoros\ServerRequestFactory;
use Psr\Http\Message\ServerRequestInterface;

return [
    SellerRepoInterface::class => DI\autowire(SellerRepository::class),
    OrderRepoInterface::class => DI\autowire(OrderRepository::class),

    ServerRequestInterface::class => ServerRequestFactory::fromGlobals(
        $_SERVER,
        $_GET,
        $_POST,
        $_COOKIE,
        $_FILES
    ),
];
