<?php

declare(strict_types=1);

use DI\ContainerBuilder;

require '../vendor/autoload.php';

$containerBuilder = new ContainerBuilder();
//$containerBuilder->useAttributes(true);

$containerBuilder->addDefinitions(__DIR__ . DIRECTORY_SEPARATOR . 'services.php');

try {
    return $containerBuilder->build();
} catch (Exception $e) {
}
