<?php

use App\Controllers\CustomerController;
use App\Controllers\SellerController;
use FastRoute\RouteCollector;
use function FastRoute\simpleDispatcher;

return simpleDispatcher(function (RouteCollector $r) {
    $r->get('/api/v1/list_orders', [SellerController::class, 'listOrders']);
    $r->get('/api/v1/info_order/{id:\d+}', [SellerController::class, 'infoOrder']);
    $r->post('/api/v1/create_order', [CustomerController::class, 'createOrder']);
    $r->get('/api/v1/shipping_costs/{address}', [CustomerController::class, 'shippingCosts']);
});
