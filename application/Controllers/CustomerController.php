<?php

declare(strict_types=1);

namespace App\Controllers;

use App\Domains\Repositories\OrderRepository;
use App\Services\ShippingCostCalculation;
use Laminas\Diactoros\Response\JsonResponse;
use Laminas\Diactoros\ServerRequest;
use OpenApi\Annotations as OA;
use Throwable;

class CustomerController
{
//    #[Inject]
    private ShippingCostCalculation $cost;

//    #[Inject]
    private OrderRepository $orderRepo;

    public function __construct(ShippingCostCalculation $cost, OrderRepository $orderRepo)
    {
        $this->cost = $cost;
        $this->orderRepo = $orderRepo;
    }

    /**
     * @OA\Get(
     *     path="/api/v1/shipping_costs/{address}",
     *     summary="shipping_costs",
     *     description="shipping_costs",
     *     operationId="shipping_costs",
     *     tags={"shipping_costs"},
     *     @OA\Parameter(
     *         name="address",
     *         in="query",
     *         description="address",
     *         required=true,
     *         @OA\Schema(
     *             type="string"
     *         )
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="OK"
     *     ),
     *     @OA\Response(
     *         response=500,
     *         description="Internal Server Error"
     *     )
     * )
     */
    public function shippingCosts(ServerRequest $request): JsonResponse
    {
        try {
            $address = $request->getAttribute("address");
            $cost = $this->cost->calculate($address);

            return new JsonResponse([
                'cost' => $cost,
            ]);
        } catch (Throwable $e) {
            return new JsonResponse([
                'error massage' => $e->getMessage(),
            ], 500);
        }
    }

    /**
     * @OA\Post(
     *     path="/api/v1/create_order",
     *     summary="create order",
     *     description="create order",
     *     operationId="createOrder",
     *     tags={"createOrder"},
     *     @OA\RequestBody(
     *         @OA\MediaType(
     *             mediaType="application/json",
     *             @OA\Schema(
     *                 @OA\Property(
     *                     property="customer_id",
     *                     description="customer id",
     *                     type="string"
     *                 ),
     *                 @OA\Property(
     *                     property="total_amount",
     *                     description="Amount",
     *                     type="string"
     *                 ),
     *                 @OA\Property(
     *                     property="status",
     *                     description="Status",
     *                     type="string"
     *                 ),
     *                 example={"customer_id": "HrdUQY4HH12", "total_amount": "123", "status": "1"}
     *             )
     *         )
     *     ),
     *     @OA\Response(
     *         response=201,
     *         description="CREATED"
     *     ),
     *     @OA\Response(
     *         response=500,
     *         description="Internal Server Error"
     *     )
     * )
     */
    public function createOrder(ServerRequest $request): JsonResponse
    {
        try {
            $this->orderRepo->create($request);

            return new JsonResponse([
                'massage' => "Order created",
            ], 201);
        } catch (Throwable $e) {
            return new JsonResponse([
                'massage' => $e->getMessage(),
            ], status: 500);
        }
    }
}
