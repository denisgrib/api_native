<?php

declare(strict_types=1);

namespace App\Controllers;

use App\Domains\Repositories\SellerRepository;
use Laminas\Diactoros\Response\JsonResponse;
use Laminas\Diactoros\ServerRequest;
use OpenApi\Annotations as OA;
use Throwable;

class SellerController
{
    /**
     * @OA\Get(
     *     path="/api/v1/info_order/{orderId}",
     *     summary="info_order",
     *     description="info_order",
     *     operationId="info_order",
     *     tags={"info_order"},
     *     @OA\Parameter(
     *         name="orderId",
     *         in="query",
     *         description="order id",
     *         required=true,
     *         @OA\Schema(
     *             type="integer",
     *             format="int32"
     *         )
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="OK"
     *     ),
     *     @OA\Response(
     *         response=500,
     *         description="Internal Server Error"
     *     )
     * )
     */
    public function infoOrder(ServerRequest $request): JsonResponse
    {
        try {
            $id = (int)$request->getAttribute("id");
            $info = SellerRepository::getInfoOrder($id);

            return new JsonResponse($info);
        } catch (Throwable $e) {
            return new JsonResponse([
                'massage' => $e->getMessage(),
            ], 500);
        }
    }

    /**
     * @OA\Get(
     *     path="/api/v1/list_orders",
     *     summary="list_orders",
     *     description="list_orders",
     *     operationId="list_orders",
     *     tags={"list_orders"},
     *     @OA\Response(
     *         response=200,
     *         description="OK"
     *     ),
     *     @OA\Response(
     *         response=500,
     *         description="Internal Server Error"
     *     )
     * )
     */
    public function listOrders(): JsonResponse
    {
        try {
            $list = SellerRepository::getListOrders();

            return new JsonResponse(data: $list);
        } catch (Throwable $e) {
            return new JsonResponse(data: [
                'massage' => $e->getMessage(),
            ], status: 500);
        }
    }
}
