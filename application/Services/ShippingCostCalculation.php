<?php

declare(strict_types=1);

namespace App\Services;

use App\Domains\Interfaces\OrderRepoInterface;
use Exception;

class ShippingCostCalculation
{
    private OrderRepoInterface $orderRepo;

    public function __construct(OrderRepoInterface $orderRepo)
    {
        $this->orderRepo = $orderRepo;
    }

    /**
     * @throws Exception
     */
    public function calculate(string $address): float
    {
        $order = $this->orderRepo::getInfoOrderByAddress($address);

        if (array_key_exists('cost', $order)) {
            return (float)$order['cost'];
        }

        throw new Exception('undefined array key');
    }
}
