<?php

declare(strict_types=1);

namespace App\Domains;

class Courier
{
    public function __construct(
        private string $name,
        private string $vehicle_type,
        private string $package_capacity
    ) {
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getVehicleType(): string
    {
        return $this->vehicle_type;
    }

    public function getPackageCapacity(): string
    {
        return $this->package_capacity;
    }

    public function deliverPackage($package): void
    {
        // delivery logic goes here
    }
}
