<?php

declare(strict_types=1);

namespace App\Domains\Repositories;

use App\Domains\Interfaces\OrderRepoInterface;
use App\Domains\Order;
use Laminas\Diactoros\ServerRequest;

class OrderRepository implements OrderRepoInterface
{
    /**
     * @return array
     */
    public static function getInfoOrderByAddress(string $address): array
    {
        if ($address === Order::EXPECTED_ADDRESS) {
            return [
                'name' => 'Ivan Ivanov',
                'order' => "Order 555",
                "address" => $address,
                'cost' => Order::EXPECTED_SHIPPING_COST,
            ];
        }

        if ($address === "minsk_pushkina_34") {
            return [
                'name' => 'Ivan Ivanov',
                'order' => "Order 444",
                "address" => $address,
                'cost' => '456.78',
            ];
        }

        return [];
    }

//    public static function getOrders()
//    {
//    }
//
//    public static function updateStatus($id, $status)
//    {
//    }

    public function create(ServerRequest $request): void
    {
        $data = json_decode($request->getBody()->getContents(), false);

        $order = new Order(
            $data->customer_id,
            $data->total_amount,
            $data->status,
        );
//        $order->save();
    }
}
