<?php

declare(strict_types=1);

namespace App\Domains\Repositories;

use App\Domains\Interfaces\SellerRepoInterface;
use App\Domains\Order;

class SellerRepository implements SellerRepoInterface
{
    public static function getInfoOrder(int $id): array
    {
        return [
            'name' => 'Ivan Ivanov',
            'order' => "Order {$id}",
            'cost' => Order::EXPECTED_SHIPPING_COST,
        ];
    }

    public static function getListOrders(): array
    {
        return [
            'order1' => 'value 1',
            'order2' => 'value 2',
            'order3' => 'value 3',
            'order4' => 'value 4',
        ];
    }
}
