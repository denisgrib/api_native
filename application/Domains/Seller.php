<?php

declare(strict_types=1);

namespace App\Domains;

class Seller
{
    public function __construct(private string $name)
    {
    }

    public function getName(): string
    {
        return $this->name;
    }
}
