<?php

declare(strict_types=1);

namespace App\Domains;

class Order
{
    public const EXPECTED_ADDRESS = 'moscow_pushkina_12';
    public const EXPECTED_SHIPPING_COST = 123.45;

    private int $id;
    private string $customer_id;
    private string $order_date;
    private string $total_amount;
    private string $status;

    public function __construct(string $customer_id, string $total_amount, string $status)
    {
        $this->customer_id = $customer_id;
        $this->total_amount = $total_amount;
        $this->status = $status;
        $this->order_date = date('Y-m-d H:i:s');
    }

    public function getStatus(): string
    {
        return $this->status;
    }

    public function getTotalAmount(): string
    {
        return $this->total_amount;
    }

    public function getOrderDate(): string
    {
        return $this->order_date;
    }

    public function getCustomerId(): string
    {
        return $this->customer_id;
    }

    public function getId(): int
    {
        return $this->id;
    }
}
