<?php

declare(strict_types=1);

namespace App\Domains\Interfaces;

interface SellerRepoInterface
{
    public static function getInfoOrder(int $id): array;

    public static function getListOrders(): array;
}
