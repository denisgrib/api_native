<?php

declare(strict_types=1);

namespace App\Domains\Interfaces;

use Laminas\Diactoros\ServerRequest;

interface OrderRepoInterface
{
//    public static function getOrders();
//    public static function updateStatus($id, $status);
    public function create(ServerRequest $request);

    public static function getInfoOrderByAddress(string $address): array;
}
