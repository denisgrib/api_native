<?php

declare(strict_types=1);

namespace App\Domains;

class Customer
{
    public function __construct(
        private int $id,
        private string $name,
        private string $email,
        private string $phone
    ) {
    }

    // Getters and Setters
    public function getId(): int
    {
        return $this->id;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getEmail(): string
    {
        return $this->email;
    }

    public function getPhone(): string
    {
        return $this->phone;
    }
}
