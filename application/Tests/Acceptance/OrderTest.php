<?php

declare(strict_types=1);

namespace App\Tests\Acceptance;

use App\Domains\Order;
use GuzzleHttp\Client;
use PHPUnit\Framework\TestCase;

class OrderTest extends TestCase
{
    private Client $client;

    public function testPostCreate(): void
    {
        $data = [
            "customer_id" => "HrdUQY4HH123",
            "total_amount" => "123",
            "status" => "1",
        ];

        $response = $this->client->post('/api/v1/create_order', [
            'body' => json_encode($data),
        ]);

        $content = $response->getBody()->getContents();
        $this->assertEquals('{"massage":"Order created"}', $content);

        $this->assertEquals(201, $response->getStatusCode());
    }

    public function testGetShippingCost(): void
    {
        $response = $this->client->get('/api/v1/shipping_costs/' . Order::EXPECTED_ADDRESS);

        $this->assertEquals(200, $response->getStatusCode());

        $content = $response->getBody()->getContents();
        $this->assertEquals('{"cost":' . Order::EXPECTED_SHIPPING_COST . '}', $content);
    }

    public function testGetShippingCost2(): void
    {
        $response = $this->client->get('/api/v1/shipping_costs/minsk_pushkina_34');

        $this->assertEquals(200, $response->getStatusCode());

        $content = $response->getBody()->getContents();
        $this->assertEquals('{"cost":456.78}', $content);
    }

    public function testGetShippingCostBad(): void
    {
        $response = $this->client->get('/api/v1/shipping_costs/bad', ['http_errors' => false]);

        $this->assertEquals(500, $response->getStatusCode());
    }

    public function testGetInfoOrder(): void
    {
        $response = $this->client->get('/api/v1/info_order/12');

        $this->assertEquals(200, $response->getStatusCode());
    }

    public function testGetInfoOrderBad(): void
    {
        $response = $this->client->get('/api/v1/info_order/bad', ['http_errors' => false]);

        $this->assertEquals(404, $response->getStatusCode());
    }

    public function testGetListOrders(): void
    {
        $response = $this->client->get('/api/v1/list_orders');

        $this->assertEquals(200, $response->getStatusCode());
    }

    protected function setUp(): void
    {
        $this->client = new Client([
            'base_uri' => 'http://api',
            'exceptions' => false,
        ]);
    }
}
