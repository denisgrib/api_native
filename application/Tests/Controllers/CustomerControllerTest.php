<?php

declare(strict_types=1);

namespace App\Tests\Controllers;

use App\Controllers\CustomerController;
use App\Domains\Order;
use App\Domains\Repositories\OrderRepository;
use App\Services\ShippingCostCalculation;
use Exception;
use Laminas\Diactoros\Response\JsonResponse;
use Laminas\Diactoros\ServerRequest;
use PHPUnit\Framework\TestCase;

class CustomerControllerTest extends TestCase
{
    public function testShippingCostCalculation()
    {
        // Create a mock ServerRequest object with a valid address.
        $request = $this->getMockBuilder(ServerRequest::class)
            ->getMock();
        $request->method('getAttribute')
            ->with('address')
            ->willReturn(Order::EXPECTED_ADDRESS);

        // Create a mock ShippingCost object.
        $cost = $this->getMockBuilder(ShippingCostCalculation::class)
            ->setConstructorArgs([(new OrderRepository())])
            ->getMock();
        $cost->method('calculate')
            ->willReturn(Order::EXPECTED_SHIPPING_COST);

        // Create the object we want to test.
        $shipping = new CustomerController($cost, (new OrderRepository()));

        // Call the function we want to test.
        $response = $shipping->shippingCosts($request);

        // Assert that the response is a JsonResponse object.
        $this->assertInstanceOf(JsonResponse::class, $response);

//        $this->assertArrayHasKey('cost', $orderJson);
//        $this->assertIsFloat($orderJson['cost']);

        // Assert that the cost value is correct.
        $this->assertEquals(
            ['cost' => Order::EXPECTED_SHIPPING_COST],
            json_decode($response->getBody()->getContents(), true)
        );
    }

    public function testShippingCostsException()
    {
        // Create a mock ServerRequest object.
        $request = $this->getMockBuilder(ServerRequest::class)->getMock();

        $request->method('getAttribute')
            ->with('address')
            ->willThrowException(new Exception('Invalid address'));

        // Create a mock ShippingCost object.
        $cost = $this->getMockBuilder(ShippingCostCalculation::class)
            ->setConstructorArgs([(new OrderRepository())])
            ->getMock();
        $cost->method('calculate')
            ->willThrowException(new Exception('Should not be called'));

        // Create the object we want to test.
        $shipping = new CustomerController($cost, (new OrderRepository()));

        // Call the function we want to test.
        $response = $shipping->shippingCosts($request);

        // Assert that the response is a JsonResponse object.
        $this->assertInstanceOf(JsonResponse::class, $response);

        // Assert that the error message is correct.
        $this->assertEquals(['error massage' => 'Invalid address'], json_decode($response->getBody()->getContents(), true));
    }
}
