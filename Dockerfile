# docker image ls -a
# docker system prune -a

#add to sudo vim /etc/docker/daemon.json
#{
#  "dns": ["8.8.8.8"]
#}

FROM php:8.1-fpm-alpine3.17

WORKDIR /var/www
COPY application /var/www

#WORKDIR /var/www/html
#
#RUN apk add --no-cache --repository http://dl-cdn.alpinelinux.org/alpine/edge/community/ --allow-untrusted gnu-libiconv
#ENV LD_PRELOAD /usr/lib/preloadable_libiconv.so php
#
#ENV PHP_MEMORY_LIMIT=1G
#ENV PHP_UPLOAD_MAX_FILESIZE: 512M
#ENV PHP_POST_MAX_SIZE: 512M
#
#RUN docker-php-ext-install pdo
#
#RUN apk add --no-cache libpng libpng-dev && docker-php-ext-install gd && apk del libpng-dev
#
#RUN apk update \
#    && apk upgrade \
#    && apk add --no-cache \
#        freetype \
#        libpng \
#        libjpeg-turbo \
#        freetype-dev \
#        libpng-dev \
#        jpeg-dev \
#        libwebp-dev \
#        libjpeg \
#        libjpeg-turbo-dev
#
#RUN docker-php-ext-configure gd \
#        --with-freetype=/usr/lib/ \
#        --with-jpeg=/usr/lib/ \
#        --with-webp=/usr
#
#RUN NUMPROC=$(grep -c ^processor /proc/cpuinfo 2>/dev/null || 1) \
#    && docker-php-ext-install -j${NUMPROC} gd
#
#
#RUN apk add --no-cache sqlite-libs
#RUN apk add --no-cache icu sqlite git openssh zip
#RUN apk add --no-cache --virtual .build-deps icu-dev libxml2-dev sqlite-dev curl-dev
#RUN docker-php-ext-install \
#        bcmath \
#        curl \
#        ctype \
#        intl \
#        pdo \
#        pdo_sqlite \
#        xml
#RUN apk del .build-deps
#
#RUN docker-php-ext-enable pdo_sqlite

# Add xdebug --no-cache
RUN apk add --virtual .build-deps $PHPIZE_DEPS
#RUN apk add --update linux-headers
RUN pecl install xdebug-3.1.5
RUN docker-php-ext-enable xdebug
RUN apk del -f .build-deps

# Configure Xdebug
RUN echo "xdebug.start_with_request=yes" >> /usr/local/etc/php/conf.d/xdebug.ini \
    #&& echo "xdebug.mode=debug" >> /usr/local/etc/php/conf.d/xdebug.ini \
    #&& echo "xdebug.log=/var/www/html/xdebug/xdebug.log" >> /usr/local/etc/php/conf.d/xdebug.ini \
    && echo "xdebug.discover_client_host=1" >> /usr/local/etc/php/conf.d/xdebug.ini \
    && echo "xdebug.client_port=9003" >> /usr/local/etc/php/conf.d/xdebug.ini \
    && echo "xdebug.mode=coverage" >> /usr/local/etc/php/conf.d/xdebug.ini

#RUN apk add postgresql postgresql-dev \
#  &amp;&amp; docker-php-ext-configure pgsql -with-pgsql=/usr/local/pgsql \
#  &amp;&amp; docker-php-ext-install pdo pdo_pgsql pgsql

#RUN apk add zlib-dev git zip \
#  && docker-php-ext-install zip

#RUN curl -sS https://getcomposer.org/installer | php \
#        && mv composer.phar /usr/local/bin/ \
#        && ln -s /usr/local/bin/composer.phar /usr/local/bin/composer

RUN curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer

RUN composer install
RUN composer dumpautoload --optimize

#ENV COMPOSER_ALLOW_SUPERUSER=1


ENV PATH="/usr/local/bin:./vendor/bin:${PATH}"

#RUN COMPOSER_MEMORY_LIMIT=-1 composer install --no-ansi --no-dev --no-interaction --no-progress --no-scripts --optimize-autoloader  && \
#    composer dump-autoload



# == nodejs ===========

